using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    public float velocityX = 10f;
    private Rigidbody2D rb;
    private MegamanController megamanController;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        megamanController = FindObjectOfType<MegamanController>();
        Destroy(gameObject,3f);
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = Vector2.right * velocityX;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Enemy")
        {
            Destroy(this.gameObject);
        }
    }
}
