using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamanController : MonoBehaviour
{
    public GameObject BulletSmallRight;
    public GameObject BulletMediumRight;
    public GameObject BulletBigRight;

    private float blinkRate = 0.01f;
    private float fixedDeltaTime;
    private Color originalColor;

    private bool isReload = false;
    private bool puedeSaltar = false;

    private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        originalColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {

        //BALAS
        if(Input.GetKeyDown(KeyCode.X))
        {
            setRunAttack();
            var position = new Vector2(transform.position.x + 1, transform.position.y);
            Instantiate(BulletSmallRight, position, BulletSmallRight.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Z) && !isReload)
		{
            setRunAttack();
            StartCoroutine(SwitchColor());
            isReload = true;
            Invoke("EndReloadMediumBullet", 3);
		}

        if (Input.GetKeyDown(KeyCode.C) && !isReload)
		{
            setRunAttack();
			StartCoroutine(SwitchColor());
			isReload = true;
			Invoke("EndReloadBigBullet", 5);
		}

        //DESPLAZAR
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(7,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-7,rb2d.velocity.y);
        }

        //SALTAR
        if (Input.GetKey(KeyCode.Space))
        {
            setJumpAnimation();
            if(puedeSaltar){
                float upSpeed = 25;
                rb2d.velocity = Vector2.up * upSpeed;
            }
            puedeSaltar = false;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Floor")
        {
            puedeSaltar = true;
        }
    }

    IEnumerator SwitchColor()
    {
        int t = 30;
        while(t>0){
            if(sr.color == originalColor)
            {
                sr.color = Color.yellow;
                yield return new WaitForSeconds(t * blinkRate);
            }
            else{
                sr.color = originalColor;
                yield return new WaitForSeconds(t * blinkRate);
            }
            t--;
        }
        sr.color = originalColor;
    }

    private void EndReloadMediumBullet()
    {
        sr.color = originalColor;
        var position = new Vector2(transform.position.x + 1, transform.position.y);
        Instantiate(BulletMediumRight, position, BulletMediumRight.transform.rotation);
        isReload=false;
    }

    private void EndReloadBigBullet()
    {
        sr.color = originalColor;
        var position = new Vector2(transform.position.x + 1, transform.position.y);
        Instantiate(BulletBigRight, position, BulletBigRight.transform.rotation);
        isReload = false;
    }

    private void setIdleAnimation(){
        _animator.SetInteger("State",0);
    }

    private void setRunAnimation(){
        _animator.SetInteger("State",1);
    }

     private void setJumpAnimation(){
       _animator.SetInteger("State",2);
    }

    private void setRunAttack(){
        _animator.SetInteger("State",3);
    }

}
